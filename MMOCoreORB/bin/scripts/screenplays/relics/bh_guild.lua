BHGuildPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "BHGuildPlay"
}

registerScreenPlay("BHGuildPlay", true)

function BHGuildPlay:start()
	if (isZoneEnabled("naboo")) then
		self:spawnMobiles()
    		self:spawnSceneObjects()
	end
end

function BHGuildPlay:spawnSceneObjects()

	--spawnSceneObject("tatooine", "object/building/tatooine/capitol_tatooine.iff", 2258, 0, 4287, 0, 1, 0, 0, 0) -- Twin Suns Capitol
	-- spawnSceneObject("tatooine", "object/tangible/event_perk/jawas_and_droids.iff", 2468, 0, 4124, 0, 1, 0, 0, 0) -- Twin Suns perk
	-- spawnSceneObject("naboo", "object/tangible/event_perk/lambda_shuttle.iff", -6252.08, 6.35, -3677.48, 0, 1, 0, 0, 0) -- Gala perk
	-- spawnSceneObject("naboo", "object/tangible/event_perk/wedding_garden_imperial_large_theater.iff", -1154.72, 13, 2782.88, 0, 1, 0, 0, 0) -- Solleu perk
	-- spawnSceneObject("dantooine", "object/tangible/event_perk/wedding_garden_rebel_large_theater.iff", 4862.22, 25, -3302.62, 0, 1, 0, 0, 0) -- Tantive perk
	-- spawnSceneObject("corellia", "object/tangible/event_perk/wrecked_sandcrawler.iff", 1768.33, 3.09, -243.30, 0, math.rad(90) ) -- LoL Outpost perk 1768.33 3.09 -243.30
	-- spawnSceneObject("lok", "object/tangible/event_perk/universe_flags.iff", -287.92, 11.92, -3087.56, 0, 0) -- NF Outpost Outpost perk
	-- spawnSceneObject("tatooine", "object/tangible/event_perk/filler_building_naboo_theed_style_9.iff", -5252, 49, 5905, 0, math.rad(45) ) -- Esandis Outpost perk
	-- spawnSceneObject("tatooine", "object/tangible/event_perk/banner_imperial_style_01.iff", 5850.14, 38.04, 4384.39, 0, math.rad(90) ) -- Mortem Solis Outpost perk
	-- spawnSceneObject("tatooine", "object/tangible/event_perk/banner_imperial_style_01.iff", 5850.14, 39.14, 4360.25, 0, math.rad(90) ) -- Mortem Solis Outpost perk  39.14 4360.25
	-- spawnSceneObject("dantooine", "object/tangible/event_perk/destroyed_atat_theater.iff", -5546, 16, 7214, 0, math.rad(90) ) -- Avalon city perk
	-- spawnSceneObject("talus", "object/tangible/event_perk/graveyard.iff",  -5260.6, 50.0, 4575.2, 0, math.rad(90) ) -- Talon city perk graveyard
	-- spawnSceneObject("tatooine", "object/tangible/terminal/terminal_bazaar.iff",  -5251.6, 56.3, 6212.7, 0, math.rad(0) ) -- Esandis bazaar terminal
	-- spawnSceneObject("corellia", "object/tangible/event_perk/imperial_landing_party_atat_theater.iff", -1423.19,16.44,-5482.19, 0, math.rad(0) ) -- Shin City Perk
	
end

function BHGuildPlay:spawnMobiles()

	--Theater NPCs
	  -- spawnMobile("lok", "junk_dealer", 60, -284.0, 12.0, -3019.8, -1, 0) -- NF Outpost
	  -- spawnMobile("lok", "informant_npc_lvl_3", 60, -299.0, 12.0, -3019.8, -1, 0) -- NF Outpost
	  -- spawnMobile("tatooine", "junk_dealer", 60, -5244, 54.9, 6212.7, -1, 0) -- Esandis
	  -- spawnMobile("tatooine", "informant_npc_lvl_3", 60, -5259.5, 53.0, 6212.7, -1, 0) -- Esandis
	  -- spawnMobile("talus", "junk_dealer", 60, -5347.75, 74.76, 4475.60, -1, 0) -- Talon
	  -- spawnMobile("talus", "informant_npc_lvl_3", 1, -5347.75, 74.76, 4460.12, -1, 0) -- Talon
	  -- spawnMobile("naboo", "junk_dealer", 60, -6315.75, 9.42, -3636.34, -1, 0) -- Gala
	  -- spawnMobile("naboo", "informant_npc_lvl_3", 1, -6315.75, 8.90, -3651.75, -1, 0) -- Gala
	  -- spawnMobile("tatooine", "junk_dealer", 60, -3372.18, 0.00, -4532.25, -1, 0) -- 94
	  -- spawnMobile("tatooine", "informant_npc_lvl_3", 60, -3386.70, 0.00, -4532.25, -1, 0) -- 94
	  -- spawnMobile("dantooine", "junk_dealer", 60, 4756.25, 4.19, -3116.82, -1, 0) -- Tantive
	  -- spawnMobile("dantooine", "informant_npc_lvl_3", 60, 4756.25, 3.06,-3131.69, -1, 0) -- Tantive
	 -- spawnMobile("tatooine", "junk_dealer", 60, 2468.30, 0.00, 4219.75, -1, 0) -- Twin Suns
	  -- spawnMobile("tatooine", "informant_npc_lvl_3", 60, 2483.73, 0.00, 4219.75, -1, 0) -- Twin suns
	  -- spawnMobile("dantooine", "junk_dealer", 60, -5820.46, 2.43, 7204.12, -1, 0) -- Avalon
	  -- spawnMobile("dantooine", "informant_npc_lvl_3", 60, -5820.46, 0.95,7187.96, -1, 0) -- Avalon
	  -- spawnMobile("corellia", "junk_dealer", 60, -1652.14, 17.18, -5516.20, -1, 0) -- Shin
	  -- spawnMobile("corellia", "informant_npc_lvl_3", 60, -1652.14, 16.37, -5532.56, -1, 0) -- Shim
	  -- spawnMobile("corellia", "junk_dealer", 60, -90.40, 29.99, -5748.24, -1, 0) -- New Alderaan
	  -- spawnMobile("corellia", "informant_npc_lvl_3", 60, -77.67, 30.07, 5748.24, -1, 0) -- New Alderan
	  
end