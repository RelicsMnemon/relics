-- Custom Screenplays


-- Tatooine

-- Caves
includeFile("custom_screenplays/tatooine/caves/tatooine_darklighter_cave.lua")
includeFile("custom_screenplays/tatooine/caves/tatooine_legacy_cult_cave.lua")
includeFile("custom_screenplays/tatooine/caves/tatooine_sand_splitters_cave.lua")

includeFile("custom_screenplays/tatooine/anchorhead_sennex_bunker.lua")
includeFile("custom_screenplays/tatooine/mos_eisley_battle_droids.lua")
includeFile("custom_screenplays/tatooine/mos_eisley_outskirts.lua")
includeFile("custom_screenplays/tatooine/mos_eisley_shinns_bunker.lua")
includeFile("custom_screenplays/tatooine/mos_entha_sand_pirate_bunker.lua")
includeFile("custom_screenplays/tatooine/mos_entha_sennex_bunker.lua")
includeFile("custom_screenplays/tatooine/valarian_command_bunker.lua")
includeFile("custom_screenplays/tatooine/valarian_dune_bunker.lua")
includeFile("custom_screenplays/tatooine/valarian_hacker_bunker.lua")
includeFile("custom_screenplays/tatooine/valarian_podracer_bunker.lua")
